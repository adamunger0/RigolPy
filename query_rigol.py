# Bring in the VISA library
import visa

# Create a resource manager
resources = visa.ResourceManager('@py')

# Open the Rigol by name. (Change this to the string for your instrument)
oscilloscope = resources.open_resource('USB0::6833::1230::DS1ZA192611545::0::INSTR')

# Return the Rigol's ID string to tell us it's there
print(oscilloscope.query("*TST?"))

# Select channel 1
oscilloscope.query(':MEAS:SOUR:CHAN1')

# Read the RMS voltage on that channel
fullreading = oscilloscope.query(':MEAS:ITEM? VRMS,CHAN1')

# Extract the reading from the resulting string...
readinglines = fullreading.splitlines()
# ...and convert it to a floating point value.
reading = float(readinglines[0])

# Send the reading to the terminal
print(reading)

#oscilloscope.query(':RUN')

# Close the connection
oscilloscope.close()

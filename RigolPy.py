#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Main file for a Rigol VISA USB scope interface. Tested on and built for a DS1054Z.

File: RigolPy.py
Author: Adam Unger
"""

import sys
from PyQt5.QtWidgets import QWidget, QApplication, QApplication, QMainWindow, QLineEdit, QLabel, QGridLayout, QRadioButton, QHBoxLayout
from PyQt5.QtGui import QDoubleValidator, QIcon

class RigolPy(QMainWindow):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.resize(500, 600)
        self.setWindowTitle('RigolPy')
        self.setWindowIcon(QIcon('./icons/oscilloscope.png'))

        self.statusBar().showMessage('Ready to start scoping...')
        self.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    rigolPy = RigolPy()
    sys.exit(app.exec_())
